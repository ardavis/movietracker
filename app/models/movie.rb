class Movie < ApplicationRecord

  # Associations
  belongs_to :rating

  has_many :movie_genres
  has_many :movie_directors
  has_many :user_movies
  has_many :genres, through: :movie_genres
  has_many :directors, through: :movie_directors
  has_many :users, through: :user_movies

  # Creates getter/setters
  # attr_accessor :imdb_id # No longer needed now that it's a column in the db

  validates :imdb_id, uniqueness: true
  validates :title, presence: true

  #Return an array to be used by the graph gem. Counts the metascores in groups of 10.
  def self.metascore_counts
    score_count = Array.new(10,0)
    result = Array.new()
    counter_label = 0

    #Get all the movies
    all.each do |movie|
      if movie.metascore < 100
        #At the index(mod 10) add 1 to the count of how many movies are within that range.
        score_count[movie.metascore/10] += 1
      else
        #Edge case of a metascore being 100 (Yeah right...)
        score_count[9] += 1
      end
    end

    #for 10 iterations...
    10.times do |index|
      #Add to the 'result' array the required items for the graph gem. Format: ["Label", value]
      result << ["#{counter_label} to #{counter_label + 10}", score_count[index]]
      counter_label += 10
    end
    #Return the result
    result
  end

  #Returns an array of release dates to be used by graph gem. Will do so in year spans of 10 if they exist.
  def self.release_dates
    #Initialize a hash and an array to store result values
    dates_count = Hash.new()
    result = Array.new()

    #For each movie, count the amount of years total if they already exist. if they don't, set the key with a value of 1.
    all.each do |movie|
      year = movie.date.try(:year)
      if dates_count.key?(year)
        dates_count[year] += 1
      else
        dates_count[year] = 1
      end
    end

    #However big the hash is, shovel it into the results array with it's key value pair.
    dates_count.length.times do |index|
      result << [dates_count.keys[index],dates_count[dates_count.keys[index]]]
    end
    #Return the result
    result
  end

  #Returns an array of movie lengths to be used by graph gem. Will do so in spans of 15 minutes
  def self.movie_lengths
    #Initialize an array. index 0: Less than 1 hour. index 9: greater than 3 hours. index++ by 15 minutes each.
    movie_length_count = Array.new(10,0)
    result = Array.new()
    counter_label = 60

    #For each movie, add 1 to the index of the array if it fits within a time
    #I am very sure there is a better algorithm. I have not spent the time to figure it out as of yet. For now, this works.
    all.each do |movie|
      movie_length_count[0] += 1 if movie.length < 60
      movie_length_count[1] += 1 if movie.length >= 60 && movie.length < 75
      movie_length_count[2] += 1 if movie.length >= 75 && movie.length < 90
      movie_length_count[3] += 1 if movie.length >= 90 && movie.length < 105
      movie_length_count[4] += 1 if movie.length >= 105 && movie.length < 120
      movie_length_count[5] += 1 if movie.length >= 120 && movie.length < 135
      movie_length_count[6] += 1 if movie.length >= 135 && movie.length < 150
      movie_length_count[7] += 1 if movie.length >= 150 && movie.length < 165
      movie_length_count[8] += 1 if movie.length >= 165 && movie.length < 180
      movie_length_count[9] += 1 if movie.length > 180
    end

    #The first index.
    result << ["Less than 1 hour", movie_length_count[0]]

    #All other movie lengths counts.
    8.times do |index|
      result << ["#{counter_label} to #{counter_label + 15} minutes", movie_length_count[index+1]]
      counter_label += 15
    end

    #Last index in array.
    result << ["Over 3 hours", movie_length_count[9]]
    #Return the result
    result
  end


  def self.add(imdb_id)
    # Return the movie if it already exists
    existing_movie = Movie.find_by(imdb_id: imdb_id)
    return existing_movie if existing_movie

    response = HTTParty.get("#{OMDB_API_URL}/?i=#{imdb_id}")
    data = JSON.parse(response.body)

    # Check if the rating already exists in the db,
    # if not, create it
    rating = Rating.where(name: data['Rated']).first_or_create


    # Initialize a new movie with data from OMDB
    released_date = data['Released'] == 'N/A' ? nil : DateTime.parse(data['Released'])

    new_movie = Movie.new(imdb_id: imdb_id,
                          title: data['Title'],
                          poster: data['Poster'],
                          date: released_date,
                          length: data['Runtime'].to_i,
                          rating: rating,
                          plot: data['Plot'],
                          metascore: data['Metascore'].to_i,
                          imdb_rating: data['imdbRating'].to_f,
                          imdb_votes: data['imdbVotes'].to_i
    )

    # Split up the genres and add them to the movie
    data['Genre'].split(', ').each do |genre_name|
      genre = Genre.where(name: genre_name).first_or_create
      new_movie.genres << genre
    end

    data['Director'].split(', ').each do |director_name|
      director = Director.where(name: director_name).first_or_create
      new_movie.directors << director
    end

    new_movie.save
    new_movie
  end

  def display_name
    "#{title} (#{rating.name})"
  end

  def genre_names
    genres.collect(&:name).join(', ')
  end

  def imdb_results
    "#{imdb_rating} (#{imdb_votes} votes)"
  end

  def rating_name
    rating.name
  end

  #def director_names
  #  names = 'Director'
  #  if directors.size > 1
  #    names = names + 's'
  #  end
  #  names + ': ' + directors.collect(&:name).join(', ')
  #end
end
