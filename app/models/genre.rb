class Genre < ApplicationRecord
  has_many :movie_genres
  has_many :movies, through: :movie_genres

  def self.movie_counts
    all.map{ |genre| [genre.name, genre.movies.count]}
  end

  def to_s
    name
  end

end
